package com.makeevapps.yotawidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.RemoteViews;
import com.yotadevices.sdk.BackscreenLauncherConstants;

import java.util.Random;

public class WidgetProvider extends AppWidgetProvider {
    private final static String TAG = WidgetProvider.class.getSimpleName();
    public final static String APPWIDGET_VISIBILITY_CHANGED = "com.yotadevices.yotaphone.action" +
            ".APPWIDGET_VISIBILITY_CHANGED";
    public final static String APPWIDGET_UPDATE = "android.appwidget.action.APPWIDGET_UPDATE";
    public final static String APPWIDGET_REFRESH = "com.makeevapps.APPWIDGET_REFRESH";

    public static void update(Context context) {
        context.sendBroadcast(new Intent(WidgetProvider.APPWIDGET_UPDATE));
    }

    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        super.onReceive(context, intent);

        String action = intent.getAction();

        if (APPWIDGET_UPDATE.equals(action) || APPWIDGET_REFRESH.equals(action) ||
                APPWIDGET_VISIBILITY_CHANGED.equals(action)) {
            Log.d(TAG, "Action: " + action);
            refreshAllWidgets(context);

        } else {
            Log.e(TAG, "Other action: " + action);
        }
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId,
                                          Bundle newOptions) {
        Log.d(TAG, "onAppWidgetOptionsChanged: " + newOptions.toString());

        Bundle appWidgetOptions = appWidgetManager.getAppWidgetOptions(appWidgetId);
        int size = appWidgetOptions.getInt(BackscreenLauncherConstants.OPTION_WIDGET_SIZE, -1);
        int display = appWidgetOptions.getInt(BackscreenLauncherConstants.OPTION_WIDGET_THEME, -1);
    }

    @Override
    public void onDisabled(Context context) {
        Log.d(TAG, "onDisabled");
        super.onDisabled(context);
    }

    public void refreshAllWidgets(Context context) {
        ComponentName thisWidget = new ComponentName(context, WidgetProvider.class);

        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        int[] thisClassWidgetIds = manager.getAppWidgetIds(thisWidget);

        manager.updateAppWidget(thisClassWidgetIds, buildWidgetView(context));
    }

    public RemoteViews buildWidgetView(Context context) {
        String[] quates = context.getResources().getStringArray(R.array.quotes);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.view_widget);

        int randomIndex = new Random().nextInt(quates.length - 1);
        views.setTextViewText(R.id.quoteTextView, quates[randomIndex]);
        views.setOnClickPendingIntent(R.id.quoteTextView, getOpenQuotePendingIntent(context, randomIndex));

        views.setOnClickPendingIntent(R.id.refreshImageView, getUpdateWidgetPendingIntent(context));
        return views;
    }

    public PendingIntent getUpdateWidgetPendingIntent(Context context) {
        Intent intent = new Intent(WidgetProvider.APPWIDGET_REFRESH);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public PendingIntent getOpenQuotePendingIntent(Context context, int quoteIndex) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(MainActivity.QUOTE_INDEX, quoteIndex);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}